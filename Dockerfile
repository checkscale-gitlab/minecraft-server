
FROM openjdk:10-jre as builder
ARG REV=latest
RUN apt-get update && \
    apt-get install -y wget git

RUN mkdir -p /build && \
    cd /build && \
    wget https://hub.spigotmc.org/jenkins/job/BuildTools/lastStableBuild/artifact/target/BuildTools.jar
    
RUN cd /build && \
    java -jar BuildTools.jar --rev ${REV}


FROM openjdk:10-jre
RUN mkdir -p /minecraft

COPY --from=builder /build/craftbukkit-* /minecraft
COPY --from=builder /build/spigot-* /minecraft
RUN echo eula=true > /minecraft/eula.txt

EXPOSE 25565
EXPOSE 25575
WORKDIR /minecraft
ENV MIN_RAM_M=512
ENV MAX_RAM_M=1024
CMD ["sh","-c","SPIGOT_JAR=$(ls /minecraft | grep spigot) && java -Xms${MIN_RAM_M}M -Xmx${MAX_RAM_M}M -jar /minecraft/$SPIGOT_JAR nogui"]