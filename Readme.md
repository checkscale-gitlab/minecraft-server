# spigot server

## building

* use ```make``` or ```make server-image``` to build minecraft-server with latest version
* pass the REV variable to build different version (example: make REV=1.8)  

## building for rpi

* use make build-jar to retrieve the server jar file (as tar.gz)
* copy the minecraft.tar.gz file, the Docker-rpi file and the server.properties file to the same Dir on the rpi
* on the rpi go to the dir where the files are and run:

``` docker
 docker build -t minecraft-srv --rm -f Dockerfile-rpi .
 ```

## running

* the server can be configured by modifieing the server.properties file and mounting it to the container

``` docker
docker run -d --name minecraft-srv -p 25565:25565 -v ./server.properties:/minecraft/server.properties minecraft-srv
```

* make the server presistant by mapping the /minecraft folder to a volume

``` docker
docker voluem create minecraft_data

docker run --name minecraft-srv -p 25565:25565 --mount source=minecraft_data,target=/minecraft -v /home/pi/minecraft-server/server.properties:/minecraft/server.properties -d minecraft-srv-rpi

```

* change the server min ram and max ram by providing MIN_RAM_M and MAX_RAM_M env vars

``` docker
docker run -d --name minecraft-srv -p 25565:25565 -v /data:/minecraft -v ./server.properties:/minecraft/server.properties -e MIN_RAM_M=1024 -e MAX_RAM_M=2048 minecraft-srv
```